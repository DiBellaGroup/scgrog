# The ScGrog Package

## Introduction

Self-calibrating GROG is a technique for calculating unit kx and ky stepping GRAPPA operators (usually called Gx and Gy) which can be used in the regular GROG method.

The [original GROG method](http://www.ncbi.nlm.nih.gov/pubmed/17969027) was first published by Seiberlich et. al. in 2007 and the [self-calibrating theory](http://onlinelibrary.wiley.com/doi/10.1002/mrm.21565/abstract) for radial data was worked out by the same group in 2008.

This package contains several key components which are explained in the following sections.

### Version

The VERSION.mat file contains a single variable `VERSION` which contains a string with the version number of this package. No modifications to this file should be made without updating this number according to [semantic versioning best practices](http://semver.org/).

### Package Management

Running `ScGrog.verify` will examine the environment that ScGrog sees and ensure that any packages it depends on are available and of the correct version.

### Tests

To make sure this software works as expected in the present environment, tests have been provided and can be run by executing `ScGrog.test`.

### Docs

In the `docs/` directory a markdown file is provided explaining how to use the function this package provides.
